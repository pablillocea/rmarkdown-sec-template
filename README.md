# RMarkdown SEC Template

Minimalist xaringan theme for html presentations using UU style/colors (heavily based on RU style/colors available in https://github.com/jvcasillas/ru_xaringan).

## Showcase

![Showcase](./resources/pictures/showcase.gif)

## Usage

For now, I suggest you clone this repository and do the necessary adjustments. Just:

``` git clone https://gitlab.com/pablillocea/rmarkdown-sec-template.git ```
